//soal A
function balikString(kata) {
    var s = kata
    var balik = ''
    for (let i = kata.length - 1; i >= 0; i--) {
      balik = balik + kata[i]
    }
    return balik
  }
  console.log(balikString('abcde')) // edcba
  console.log(balikString('rusak')) // kasur
  console.log(balikString('racecar')) // racecar
  console.log(balikString('haji')) // ijah
  

//soal B
function palindrome(kata) {
    var benar = true
    var s = kata
    var balik = ''
    for (let i = kata.length - 1; i >= 0; i--) {
      balik = balik + kata[i]
    }
  
    if (benar) {
      if (balik == kata) {
        console.log('Kata ini adalah palindrome')
      } else if (balik != kata) {
        console.log('Kata ini adalah bukan palindrome')
      }
    }
  }
  
  console.log(palindrome('kasur rusak')) // true
  console.log(palindrome('haji ijah')) // true
  console.log(palindrome('nabasan')) // false
  console.log(palindrome('nababan')) // true
  console.log(palindrome('jakarta')) // false
  
  //soal C
  function bandingkan(a, b) {
    if (a < 0 && b) {
      console.log(-1)
    } else if (b < 0 && a) {
      console.log(-1)
    } else if (a > b){
      console.log(a)
    } else if (b > a) {
      console.log(b)
    } else {
      console.log(-1)
    }
  }
  
  console.log(bandingkan(10, 15)); // 15
  console.log(bandingkan(12, 12)); // -1
  console.log(bandingkan(-1, 10)); // -1 
  console.log(bandingkan(112, 121));// 121
  console.log(bandingkan(1)); // 1
  console.log(bandingkan()); // -1
  console.log(bandingkan("15", "18")) // 18