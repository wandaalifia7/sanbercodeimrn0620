//if-else

var nama = "John"
var peran = ""

function ifElse(nama, peran) {
    if (nama == "") {
        console.log('Nama harus diisi!')
    } else if ( nama && peran == '') {
        console.log('Halo ' + nama + ', Pilih peranmu untuk memulai game')
    } else if ( nama == 'Jane' && peran == 'Penyihir') {
        console.log('Selamat datang di Dunia Werewolf, Jane\nHalo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!')
    } else if ( nama == 'Jenita' && peran == 'Guard') {
        console.log('Selamat datang di Dunia Werewolf, Jenita\nHalo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf')
    } else if ( nama == 'Junaedi' && peran == 'Werewolf') {
        console.log('Selamat datang di Dunia Werewolf, Junaedi\nHalo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!')
    }
}


console.log('============ If-Else: Blank ============')
ifElse('', '')

console.log('============ If-Else: John ============')
ifElse('John', '')

console.log('============ If-Else Jane ============')
ifElse('Jane', 'Penyihir')

console.log('============ If-Else: Jenita ============')
ifElse('Jenita', 'Guard')

console.log('============ If-Else: Junaedi ============')
ifElse('Junaedi', 'Werewolf')


//switch case

var hari = 21; 
var bulan = 1; 
var tahun = 1945;
var teksBulan;
//  Maka hasil yang akan tampil di console adalah: '21 Januari 1945';


switch(true) {
    case (hari > 31 && hari < 1): {
        console.log('Input tanggal salah')
        break;
    }
    case (tahun < 1900 && tahun > 2200): {
        console.log('input tahun salah')
        break;

    }
        default:{
        switch(true){
            case bulan == 1:
                teksBulan = 'Januari';
                break;

                case bulan == 2:
                    teksBulan = 'Februari';
                    break;

                    case bulan == 3:
                        teksBulan = 'Maret';
                        break;
                        
                        case bulan == 4:
                            teksBulan = 'April';
                            break;
            
                            case bulan == 5:
                                teksBulan = 'Mei';
                                break;
            
                                case bulan == 6:
                                    teksBulan = 'Juni';
                                    break;
                                 
                                    case bulan == 7:
                                        teksBulan = 'Juli';
                                        break;
                        
                                        case bulan == 8:
                                            teksBulan = 'Agustus';
                                            break;
                        
                                            case bulan == 9:
                                                teksBulan = 'September';
                                                break;
                                                                                                         
                                                case bulan == 10:
                                                    teksBulan = 'Oktober';
                                                    break;
                                    
                                                    case bulan == 11:
                                                        teksBulan = 'November';
                                                        break;
                                    
                                                        case bulan == 12:
                                                            teksBulan = 'Desember';
                                                            break;

                        
                                                            
                
        }
        console.log(hari, ' ', teksBulan, ' ', tahun)
    }
}