//soal 1
function range(startNum, finishNum) {

    if (startNum && finishNum == null) {
        console.log(-1);
    } else if (startNum == null && finishNum == null) {
        console.log(-1);
    } else {

        for (var i = startNum; i <= finishNum; i++) {
            var asc = [i];
            if (startNum < finishNum && finishNum > startNum) {
                console.log(asc.join());
            }
        }

        for (var j = startNum; j >= finishNum; j--) {
            var des = [j];
            if (startNum > finishNum && finishNum < startNum) {
                console.log(des.join());
            }
        }
    }
}

console.log('========= soal 1 ==========')
console.log(range(1, 10)); //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log('===================')
console.log(range(1)); // -1
console.log('===================')
console.log(range(11, 18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log('===================')
console.log(range(54, 50)); // [54, 53, 52, 51, 50]
console.log('===================')
console.log(range()) // -1 


//soal 2
function rangeWithStep(startNum, finishNum, step) {
    for (var i = startNum; i <= finishNum; i += step) {
      var s = [i]
      if (startNum < finishNum && finishNum > startNum) {
        console.log(s.join())
      }
    }
  
    for (var j = startNum; j >= finishNum; j -= step) {
      var des = [j]
      if (startNum > finishNum && finishNum < startNum) {
        console.log(des.join())
      }
    }
  }
  
  console.log('========= soal 2 ==========')
  console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
  console.log('===================')
  console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
  console.log('===================')
  console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
  console.log('===================')
  console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]

  


  //soal 3
  function sum(startNum, finishNum, step) {

    if (startNum && finishNum == null, step == null) {
        console.log(startNum);
    } else if (startNum == null && finishNum == null, step == null) {
        console.log(0);
    } else {

     for (var i = startNum; i <= finishNum; i += step) {
      var s = [i]
      if (startNum < finishNum && finishNum > startNum) {
        console.log(s.sum())
      }
        }

    for (var j = startNum; j >= finishNum; j -= step) {
      var des = [j]
      if (startNum > finishNum && finishNum < startNum) {
        console.log(des.sum())
      }
        }
    }
}


console.log('========= soal 3 ==========')
console.log(sum(1,10)) // 55
console.log('===================')
console.log(sum(5, 50, 2)) // 621
console.log('===================')
console.log(sum(15,10)) // 75
console.log('===================')
console.log(sum(20, 10, 2)) // 90
console.log('===================')
console.log(sum(1)) // 1
console.log('===================')
console.log(sum()) // 0 

