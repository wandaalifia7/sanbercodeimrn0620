//soal 1
//release 0

class Animal {
    constructor(name) {
      this._name = name
    }
    get name() {
      this._legs = 4
      this._cold_blooded = false
      return this._name
    }
    get legs() {
      this._legs = 4
      this._cold_blooded = false
      return this._legs
    }
    get cold_blooded() {
      this._cold_blooded = false
      return this._cold_blooded
    }
  }
  
  var sheep = new Animal('shaun')
  
  console.log(sheep.name) // "shaun"
  console.log(sheep.legs) // 4
  console.log(sheep.cold_blooded) // false

  
  //release 1

class Animal {
    constructor(name) {
      this._name = name
    }
    get name() {
      this._legs = 4
      this._cold_blooded = false
      return this._name
    }
    get legs() {
      this._legs = 4
      this._cold_blooded = false
      return this._legs
    }
    get cold_blooded() {
      this._cold_blooded = false
      return this._cold_blooded
    }
  }
  
  class Ape extends Animal {
  
     
  
    }
  
  class Frog extends Animal {
    
  }
  
  var sungokong = new Ape("kera sakti")
  sungokong.yell() // "Auooo"
   
  var kodok = new Frog("buduk")
  kodok.jump() // "hop hop" 